# Hello, Rust
![pipeline status](https://gitlab.com/wpm/hello-rust/badges/main/pipeline.svg)
![pipeline status](https://gitlab.com/wpm/hello-rust/badges/main/coverage.svg)

This is a "Hello, World" program written in Rust that uses Gitlab's CI/CD pipeline.
It demonstrates how to do the following for a Rust project in GitLab:
* Run tests with coverage
* Build a binary release artifact
* Generate [documentation](https://wpm.gitlab.io/hello-rust/doc/hello_rust/)

