fn main() {
    println!("Hello, world!");
}


#[cfg(test)]
mod tests {
    use crate::main;

    #[test]
    fn run_main() {
        main();
    }
}